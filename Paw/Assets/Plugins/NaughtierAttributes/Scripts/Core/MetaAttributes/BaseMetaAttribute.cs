namespace NaughtierAttributes
{
    public abstract class BaseMetaAttribute : BaseNaughtierAttribute
    {
        public int Order { get; set; }
    }
}
