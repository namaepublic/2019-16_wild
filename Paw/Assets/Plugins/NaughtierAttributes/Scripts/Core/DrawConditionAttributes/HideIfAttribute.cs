using System;

namespace NaughtierAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class HideIfAttribute : BaseDrawConditionAttribute
    {
        public string[] ConditionNames { get; private set; }

        public HideIfAttribute(params string[] conditionNames)
        {
            this.ConditionNames = conditionNames;
        }
    }
}
