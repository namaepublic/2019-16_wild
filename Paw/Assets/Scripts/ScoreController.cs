﻿using System;
using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText;
    private int m_score;

    private void Start()
    {
        m_score = 0;
        _scoreText.text = "0";
    }

    public void IncreaseScore()
    {
        m_score++;
        _scoreText.text = m_score.ToString();
    }

    public int GetScore() => m_score;
}
