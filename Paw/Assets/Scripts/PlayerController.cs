﻿using System;
using System.Linq;
using NaughtierAttributes;
using UnityEngine;

[Flags]
public enum Intent : byte
{
    Left = 0b1,
    Right = 0b10,
    Run = 0b100,
    Jump = 0b1000,
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    [Header("Components")] [SerializeField]
    private Transform _graphicsTransform;

    [SerializeField, Hide, GetComponent] private Rigidbody2D _rigidbody2D;

    [Header("Keys")] [SerializeField] private KeyCode[] _leftKeys;
    [SerializeField] private KeyCode[] _rightKeys;
    [SerializeField] private KeyCode[] _runKeys;
    [SerializeField] private KeyCode[] _jumpKeys;

    [Header("Movement stats")] [SerializeField]
    private float _walkSpeed;

    [SerializeField] private float _runSpeed;
    [SerializeField] private float _groundedLinearDrag;

    [Header("Jump stats")] [SerializeField]
    private float _jumpForce;

    [SerializeField] private float _runningJumpForce;
    [SerializeField] private float _minXVelocityToBigJump;
    [SerializeField] private float _midAirLinearDrag;

    [SerializeField] private Vector2 _groundCheckOffset;
    [SerializeField] private Vector2 _groundCheckSize;
    [SerializeField] private LayerMask _groundLayerMask;

    private Intent m_intent;

    private bool m_isGrounded;

    private Vector2 GroundCheckOffset
    {
        get
        {
            var offset = _groundCheckOffset;
            offset.x *= _graphicsTransform.localScale.x;
            return offset;
        }
    }

    private void Update()
    {
        HandleIntents();
    }

    private void FixedUpdate()
    {
        if (!(m_isGrounded = CheckGround()))
        {
            _rigidbody2D.drag = _midAirLinearDrag;
            return;
        }

        _rigidbody2D.drag = _groundedLinearDrag;

        var running = HasIntent(Intent.Run);
        var speed = running ? _runSpeed : _walkSpeed;

        var velocity = _rigidbody2D.velocity;

        if (HasIntent(Intent.Left))
        {
            FlipX(true);
            velocity.x = -speed;
        }

        if (HasIntent(Intent.Right))
        {
            FlipX(false);
            velocity.x = speed;
        }

        if (HasIntent(Intent.Jump))
        {
            velocity.y = running && Mathf.Abs(velocity.x) > _minXVelocityToBigJump ? _runningJumpForce : _jumpForce;
            RemoveIntent(Intent.Jump);
        }

        _rigidbody2D.velocity = velocity;
    }

    private void HandleIntents()
    {
        if (CheckKeysDown(_leftKeys))
        {
            RemoveIntent(Intent.Right);
            AddIntent(Intent.Left);
        }

        if (CheckKeysDown(_rightKeys))
        {
            RemoveIntent(Intent.Left);
            AddIntent(Intent.Right);
        }

        if (CheckKeysUp(_leftKeys) && !CheckKeys(_leftKeys))
        {
            RemoveIntent(Intent.Left);
            CheckAndAddIntent(_rightKeys, Intent.Right);
        }

        if (CheckKeysUp(_rightKeys) && !CheckKeys(_rightKeys))
        {
            RemoveIntent(Intent.Right);
            CheckAndAddIntent(_leftKeys, Intent.Left);
        }

        RemoveIntent(Intent.Run);
        CheckAndAddIntent(_runKeys, Intent.Run);

        if (m_isGrounded) CheckDownAndAddIntent(_jumpKeys, Intent.Jump);
    }

    public void FlipX() => FlipX(transform.localScale.x > 0);

    public void FlipX(bool faceLeft)
    {
        var scale = _graphicsTransform.localScale;
        scale.x = faceLeft ? -Mathf.Abs(scale.x) : Mathf.Abs(scale.x);
        _graphicsTransform.localScale = scale;
    }

    private bool CheckGround() =>
        Physics2D.OverlapBox(_rigidbody2D.position + GroundCheckOffset, _groundCheckSize, 0, _groundLayerMask);

    private static bool CheckKeys(KeyCode[] keys) => keys.Any(key => Input.GetKey(key));

    private static bool CheckKeysDown(KeyCode[] keys) => keys.Any(key => Input.GetKeyDown(key));

    private static bool CheckKeysUp(KeyCode[] keys) => keys.Any(key => Input.GetKeyUp(key));

    private bool HasIntent(Intent intent) => (m_intent & intent) != 0;

    private void AddIntent(Intent intent) => m_intent |= intent;

    private void RemoveIntent(Intent intent) => m_intent &= ~intent;

    private void IsolateIntent(Intent intent) => m_intent &= intent;

    private bool CheckAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (!CheckKeys(keys)) return false;
        AddIntent(intent);
        return true;
    }

    private bool CheckDownAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (!CheckKeysDown(keys)) return false;
        AddIntent(intent);
        return true;
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(_rigidbody2D.position + GroundCheckOffset, _groundCheckSize);
    }
#endif
}