﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _smoothSpeed;

    private void Start() => transform.position = _target.position + _offset;

    private void FixedUpdate() => 
        transform.position = Vector3.MoveTowards(transform.position, _target.position + _offset, _smoothSpeed * Time.deltaTime);
}