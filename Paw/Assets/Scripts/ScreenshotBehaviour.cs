﻿using System;
using System.Diagnostics;
using System.IO;
using NaughtierAttributes;
#if UNITY_EDITOR
using NaughtierAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;

[ExecuteInEditMode]
public class ScreenshotBehaviour : MonoBehaviour
{
    [SerializeField] private string _folderPath;
    [SerializeField] private string _fileName;

    public bool waitForFinished;
    [ShowIf("waitForFinished")] public bool openFolderLocation;

    [SerializeField, ShowIf("waitForFinished")]
    private bool _useScreenshotTookEvent;

    [SerializeField, ShowIf("waitForFinished", "_useScreenshotTookEvent")]
    private UnityEvent _onScreenshotTookEvent;

    private string m_filePath;
    private bool m_screenshotTook;
    private ProcessStartInfo m_processStartInfo;

    private bool m_init;

    public string FolderPath
    {
        get => _folderPath;
        set
        {
            _folderPath = value;
            SetFilePath();
        }
    }

    public string FileName
    {
        get => _fileName;
        set
        {
            _fileName = value;
            SetFilePath();
        }
    }

    public void Init()
    {
        m_init = true;
        m_processStartInfo = new ProcessStartInfo("explorer.exe");
        SetFilePath();
    }

    public void TakeScreenshot()
    {
        m_screenshotTook = true;
        if (File.Exists(m_filePath)) File.Delete(m_filePath);
        ScreenCapture.CaptureScreenshot(m_filePath);
    }

    private void Update()
    {
        if (!m_init) return;
        if (!waitForFinished || !m_screenshotTook) return;
        if (!File.Exists(m_filePath)) return;
        m_screenshotTook = false;
        if (openFolderLocation) OpenFolderLocation();
        if (_useScreenshotTookEvent) _onScreenshotTookEvent.Invoke();
    }

    private void SetFilePath()
    {
        m_filePath = Path.Combine(_folderPath, _fileName);
        if (openFolderLocation) InitFolderProcessArg();
    }

    private void OpenFolderLocation()
    {
        if (!Directory.Exists(_folderPath)) return;
        Process.Start(m_processStartInfo);
    }

    private void InitFolderProcessArg() => m_processStartInfo.Arguments = $"/select, {m_filePath}";

#if UNITY_EDITOR
    [CustomEditor(typeof(ScreenshotBehaviour))]
    private class ScreenshotDrawer : NaughtierEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var screenshotBehaviour = (ScreenshotBehaviour) target;
            try
            {
                screenshotBehaviour.Init();
                screenshotBehaviour.Update();

                GUI.enabled = !screenshotBehaviour.m_screenshotTook;
                if (GUILayout.Button("Take Screenshot"))
                    screenshotBehaviour.TakeScreenshot();
                GUI.enabled = true;

                if (screenshotBehaviour.m_screenshotTook)
                    EditorGUILayout.LabelField("Taking screenshot...");
            }
            catch
            {
                screenshotBehaviour.m_screenshotTook = false;
            }
        }
    }
#endif
}