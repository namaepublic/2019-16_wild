﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private Transform _zeroTransform;
    [SerializeField] private Transform _pawTransform;
    [SerializeField] private Transform _virtualCameraTransform;
    [SerializeField] private float _curveTimeFactor;
    [SerializeField] private float _curveValueFactor;
    [SerializeField] private AnimationCurve _cameraRotationByDistance;
    public void ReloadScene() => SceneManager.LoadScene(0);

    private void Update()
    {
        var angles = _virtualCameraTransform.eulerAngles;
        angles.z = _cameraRotationByDistance.Evaluate((_pawTransform.position.x - _zeroTransform.position.x) * _curveTimeFactor) *
                   _curveValueFactor;
        _virtualCameraTransform.eulerAngles = angles;
    }
}